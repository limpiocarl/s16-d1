// Loops

// instruction: display "Juan Dela Cruz" on our console 10x

console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");
console.log("Juan Dela Cruz");

// instruction: display each element available on our array

let students = ["TJ", "Mia", "Tin", "Chris"];
console.log(students[0]);
console.log(students[1]);
console.log(students[2]);
console.log(students[3]);

// while loop

let count = 5;

// while (/* condition*/) { //condition - evaluates a given code if is true or false - if the condition is true, the loop will start and continue our iteration or  the repetition of our block of code, but if the condition is false, the loop or the repetition will stop

//         //block of code - this will be repeated by loop

//         //counter for our iteration - this is the reason of our continuous loop/iteration
// }

/* example
        instructions: Repeat a name "Sylvan" 5x
*/

while (count !== 0) {
        console.log("Sylvan");
        count--;
}

// 

let number = 1;

while (number <= 5) {
        console.log(number);
        number++;
}

let fruits = ["Banana", "Mango"];
let index = 0;

while (index < fruits.length) {
        console.log(fruits[index]);
        index += 1;
}

// do-while - do the statement once, before going to the condition

let countA = 1;

do {
        console.log("Juan");
        countA++;
} while (countA <= 6);

let letters = ["a", "b", "c", "d"]

let indexLetters = 0;

do {
        console.log(letters[indexLetters]);
        indexLetters += 1;
} while (indexLetters < letters.length);

let indexNumber = 0;

let computerBrands = ["Apple Macbook Pro", "HP NoteBook", "Asus", "Lenovo", "Acer", "Dell", "Huawei"];

do {
        console.log(computerBrands[indexNumber]);
        indexNumber++;
} while (indexNumber <= computerBrands.length - 1);

// for loop

for (let count = 5; count != 0; count--) {
        console.log("Carl");
}

let myArray = ["theta", "beta", "gamma"];

for (index = 0; index < myArray.length; index += 1) {
        console.log(myArray[index]);
}

let colors = ["red", "green", "blue", "yellow", "purple", "white", "black"];

for (i = 0; i < colors.length; i += 1) {
        console.log(colors[i]);
}

// continue & break

// break - stops the execution of our code

// continue - skip a block code and continue to the next iteration

for (let i = 18; i <= 25; i++) {
        if (i == 21 || i == 18) {
                continue;
        }
        console.log(i);
}

let studentNames = ["Den", "Jayson", "Marvin", "Rommel"];

for (i = 0; i < studentNames.length; i += 1) {
        if (studentNames[i] == "Jayson") {
                console.log(studentNames[i]);
                break;
        }
        // console.log(studentNames[i]);
}

// activity 1

/*
Instructions:

1. Given the array 'adultAge', display only adult age range on the console.

-- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
the students must display only the given sample output below on their console.
*/

let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];

for (i = 0; i < adultAge.length; i += 1) {
  if (adultAge[i] >= 20) {
                console.log(adultAge[i]);
  } else {
    continue;
    }
}

// activity 2

/*
Instructions:

2. Given an array 'students' and a function searchStudent, create a solution that, once a function is invoked with a given name of student as its argument,
	the function will start to loop and search for the student on a given array and once there's found it will print it on the console 
  and stop the execution of loop. 
  
  -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break
*/

let students = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(studentName) { 
  for (i = 0; i < students.length; i += 1) {
                if (students[i] == studentName) { 
                        console.log(students[i]);
                        break;
    }                                 
  }
} 

searchStudent("Jazz");
